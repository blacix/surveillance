#/bin/bash

source config.sh

# TODO for all threads ( cameras )
motion_detection() {
	echo "motion_detection $1"
	curl -s $MOTION_CONTROL_URL/0/detection/$1
}

is_motion_running() {
	local status="$(curl -s $MOTION_CONTROL_URL/0/detection/status)"
	[[ $status == *"ACTIVE"* ]]
	return
}

is_user_present() {
	echo "pinging $USER_MOBILE"
	# fasetr than waiting for ping command to perform all the 3 pings, we only need one successful
	for i in {0..3}
	do
		ping -W 2 -c 1 $USER_MOBILE > /dev/null 2>&1
		[ $? -eq 0 ] && return
		sleep 1
	done

	false
	return
}




echo "motion detected command: $MOTION_DETECTED_COMMAND"

while [ true ]; do
	if ! is_user_present; then
		echo "user mobile not found"
		if ! is_motion_running; then
			echo "starting motion detection"
			motion_detection "start"
		fi
	else
		echo "user mobile found"
		if is_motion_running; then
			echo "pausing motion detection"
			motion_detection "pause"
		fi
	fi
	
	# 	# this is for a specific android app, dunno which
	# 	#curl --data ""  http://192.168.1.109:8080/enabletorch
	# 	#curl --data ""  http://192.168.1.109:8080/disabletorch

	sleep 5
done

exit 0

