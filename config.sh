USER_MOBILE=192.168.0.102
MOTION_TARGET_DIR=$(grep target_dir /etc/motion/motion.conf | cut -d " " -f2)
MOTION_DETECTED_COMMAND=$(grep on_motion_detected /etc/motion/motion.conf | grep -v "[#;]" | cut -d " " -f2)
MOTION_CONTROL_PORT=$(grep webcontrol_port /etc/motion/motion.conf | cut -d " " -f2)
MOTION_CONTROL_HOST="localhost"
MOTION_CONTROL_URL=$MOTION_CONTROL_HOST:$MOTION_CONTROL_PORT