#/bin/bash
cd $HOME/surveillance
STR=$(curl --connect-timeout 5 http://192.168.0.103:8080/list_videos | jq '.[] | .name' | sed 's/\"//g')
SAVE_DIR=$HOME/lacos/Dropbox/surveillance/$(date +"%y-%m-%d-%H")
if [ ! -d "$SAVE_DIR" ] && [ ${#STR} -gt 1 ]; then
	mkdir -p $SAVE_DIR
fi
for s in $STR
do
	wget http://192.168.0.103:8080/v/modet/$s
	curl --data ""  http://192.168.0.103:8080/remove/modet/$s
	mv -f $s $SAVE_DIR/$s
done


